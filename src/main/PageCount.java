package main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.FSDirectory;

import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * This class generates the page link map for a wikipedia dump.
 * @author Marco Jungwirth
 *
 */
public class PageCount {

	public static void main(String[] args) throws IOException, ParseException {
		String usage = "Usage:\tPageCount [-input dir] [-output f] [-log f2]\n\n";
	    if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
	      System.out.println(usage);
	      System.exit(0);
	    }
		String inputdir = "D:/clef/wikitest";
		String logfile = "D:/clef/logs/pagecount.log";
		String outputfile = "D:\\clef\\index\\pagecount.json";
		for(int i = 0;i < args.length;i++) {
	      if ("-input".equals(args[i])) {
	    	  inputdir = args[i+1];
	    	  i++;
	      } else if ("-output".equals(args[i])) {
	    	  outputfile = args[i+1];
	    	  i++;
	      } else if ("-log".equals(args[i])) {
	    	  logfile = args[i+1];
	    	  i++;
	      }
		}
		PrintWriter pw = new PrintWriter(new File(logfile));
		HashMap<String, Integer> pagecount = new HashMap<String, Integer>();
	    final Path paths = Paths.get(inputdir);
		Files.walkFileTree(paths, new SimpleFileVisitor<Path>() {
	        @Override
	        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	          if(file.toString().endsWith(".ign")){ return FileVisitResult.CONTINUE; }
	          try {
	        	File input = file.toFile();
				org.jsoup.nodes.Document doc = org.jsoup.Jsoup.parse(input, "UTF-8", "http://example.com/");
				org.jsoup.nodes.Element element = doc.select("div[id=content]").first();
				
				if(element == null)
			          return FileVisitResult.CONTINUE;
				
				org.jsoup.select.Elements links = element.select("a[href]");
				for(org.jsoup.nodes.Element link : links){
					String path = java.net.URLDecoder.decode(link.attr("href"));
					if(!path.contains("../../articles"))
						continue;
					
					String key = path.substring(12);
					if(pagecount.containsKey(key))
						pagecount.put(key,	pagecount.get(key) + 1);
					else
						pagecount.put(key, 1);
				}
	          } catch (Exception ignore) {
	             ignore.printStackTrace(pw);
	             pw.flush();
	          }
	          return FileVisitResult.CONTINUE;
	        }
	      });

		ObjectMapper mapper = new ObjectMapper();

		//Object to JSON in file
		mapper.writeValue(new File(outputfile), pagecount);
		pw.close();

	}

}

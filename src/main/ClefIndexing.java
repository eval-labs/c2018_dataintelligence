package main;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;
import org.tartarus.snowball.ext.FrenchStemmer;
import org.tartarus.snowball.ext.GermanStemmer;

import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.StopwordAnalyzerBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.WordlistLoader;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is used to generate a wikipedia index in either english, french or german.
 * @author Marco Jungwirth
 *
 */
public class ClefIndexing {
	
	static PrintWriter logpw;
	//231
	public final static CharArraySet stopSetGer = new CharArraySet(Arrays.asList(
	"aber", "alle", "allem", "allen", "aller", "alles", "als", "also", "am", "an", "ander", "andere", "anderem", "anderen", "anderer", "anderes"
	, "anderm", "andern", "anderr", "anders", "auch", "auf", "aus", "bei", "bin", "bis", "bist", "da", "damit", "dann", "der", "den", "des", "dem"
	, "die", "das", "da�", "derselbe", "derselben", "denselben", "desselben", "demselben", "dieselbe", "dieselben", "dasselbe", "dazu", "dein", "deine"
	, "deinem", "deinen", "deiner", "deines", "denn", "derer", "dessen", "dich", "dir", "du", "dies", "diese", "diesem", "diesen", "dieser", "dieses"
	, "doch", "dort", "durch", "ein", "eine", "einem", "einen", "einer", "eines", "einig", "einige", "einigem", "einigen", "einiger", "einiges", "einmal"
	, "er", "ihn", "ihm", "es", "etwas", "euer", "eure", "eurem", "euren", "eurer", "eures", "f�r", "gegen", "gewesen", "hab", "habe", "haben", "hat", "hatte"
	, "hatten", "hier", "hin", "hinter", "ich", "mich", "mir", "ihr", "ihre", "ihrem", "ihren", "ihrer", "ihres", "euch", "im", "in", "indem", "ins", "ist"
	, "jede", "jedem", "jeden", "jeder", "jedes", "jene", "jenem", "jenen", "jener", "jenes", "jetzt", "kann", "kein", "keine", "keinem", "keinen", "keiner"
	, "keines", "k�nnen", "k�nnte", "machen", "man", "manche", "manchem", "manchen", "mancher", "manches", "mein", "meine", "meinem", "meinen", "meiner"
	, "meines", "mit", "muss", "musste", "nach", "nicht", "nichts", "noch", "nun", "nur", "ob", "oder", "ohne", "sehr", "sein", "seine", "seinem", "seinen"
	, "seiner", "seines", "selbst", "sich", "sie", "ihnen", "sind", "so", "solche", "solchem", "solchen", "solcher", "solches", "soll", "sollte", "sondern"
	, "sonst", "�ber", "um", "und", "uns", "unse", "unsem", "unsen", "unser", "unses", "unter", "viel", "vom", "von", "vor", "w�hrend", "war", "waren", "warst"
	, "was", "weg", "weil", "weiter", "welche", "welchem", "welchen", "welcher", "welches", "wenn", "werde", "werden", "wie", "wieder", "will", "wir", "wird"
	, "wirst", "wo", "wollen", "wollte", "w�rde", "w�rden", "zu", "zum", "zur", "zwar", "zwischen"), false);
	//163
	public final static CharArraySet stopSetFre = new CharArraySet(Arrays.asList(
	 "au", "aux", "avec", "ce", "ces", "dans", "de", "des", "du", "elle", "en", "et", "eux", "il", "je", "la", "le", "leur", "lui", "ma", "mais", "me"
	 , "m�me", "mes", "moi", "mon", "ne", "nos", "notre", "nous", "on", "ou", "par", "pas", "pour", "qu", "que", "qui", "sa", "se", "ses", "son", "sur"
	 , "ta", "te", "tes", "toi", "ton", "tu", "un", "une", "vos", "votre", "vous", "c", "d", "j", "l", "�", "m", "n", "s", "t", "y", "�t�", "�t�e", "�t�es"
	 , "�t�s", "�tant", "suis", "es", "est", "sommes", "�tes", "sont", "serai", "seras", "sera", "serons", "serez", "seront", "serais", "serait", "serions"
	 , "seriez", "seraient", "�tais", "�tait", "�tions", "�tiez", "�taient", "fus", "fut", "f�mes", "f�tes", "furent", "sois", "soit", "soyons", "soyez"
	 , "soient", "fusse", "fusses", "f�t", "fussions", "fussiez", "fussent", "ayant", "eu", "eue", "eues", "eus", "ai", "as", "avons", "avez", "ont", "aurai"
	 , "auras", "aura", "aurons", "aurez", "auront", "aurais", "aurait", "aurions", "auriez", "auraient", "avais", "avait", "avions", "aviez", "avaient", "eut"
	 , "e�mes", "e�tes", "eurent", "aie", "aies", "ait", "ayons", "ayez", "aient", "eusse", "eusses", "e�t", "eussions", "eussiez", "eussent", "ceci", "cel� "
	 , "cet", "cette", "ici", "ils", "les", "leurs", "quel", "quels", "quelle", "quelles", "sans", "soi"), false);
	//319
	public final static CharArraySet stopSetEng = new CharArraySet(Arrays.asList(
	"a", "about", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am"
	, "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at"
	, "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between"
	, "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "computer", "con", "could", "couldnt", "cry", "de", "describe", "detail"
	, "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone"
	, "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four"
	, "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon"
	, "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "i", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself"
	, "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly"
	, "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing"
	, "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own"
	, "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side"
	, "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten"
	, "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thick"
	, "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty"
	, "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter"
	, "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with"
	, "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves"), false);
	/** Index all text files under a directory. 
	 * @throws FileNotFoundException */
	  public static void main(String[] args) throws FileNotFoundException {
	    String usage = "java org.apache.lucene.demo.IndexFiles"
	                 + " [-index INDEX_PATH] [-pt PAGETABLE_PATH] [-docs DOCS_PATH] [-update]\n";
	    String indexPath = "index";
	    CharArraySet usedStopWords = ClefIndexing.stopSetEng;
	    SnowballProgram stemmer = new EnglishStemmer();
	    String docsPath = null;
	    String ptPath = "";
	    for(int i=0;i<args.length;i++) {
	      if ("-index".equals(args[i])) {
	        indexPath = args[i+1];
	        i++;
	      } else if ("-docs".equals(args[i])) {
	        docsPath = args[i+1];
	        i++;
	      } else if ("-pt".equals(args[i])) {
	        ptPath = args[i+1];
	        i++;
		  } else if ("-log".equals(args[i])) {
	        ClefIndexing.logpw = new PrintWriter(new File("D:\\clef\\" + args[i+1]));
	        i++;
	      } else if ("-stop".equals(args[i])) {
	        switch(args[i+1]){
	        	case "fre": usedStopWords = ClefIndexing.stopSetFre; stemmer = new FrenchStemmer(); break;
	        	case "ger": usedStopWords = ClefIndexing.stopSetGer; stemmer = new GermanStemmer(); break;
	        	default: usedStopWords = ClefIndexing.stopSetEng; stemmer = new EnglishStemmer();
	        }
	        i++;
		  }
	    }
	    System.out.println(usedStopWords.size());
	    if (docsPath == null) {
	      System.err.println("Usage: " + usage);
	      System.exit(1);
	    }

	    final Path docDir = Paths.get(docsPath);
	    if (!Files.isReadable(docDir)) {
	      System.out.println("Document directory '" +docDir.toAbsolutePath()+ "' does not exist or is not readable, please check the path");
	      System.exit(1);
	    }
	    
	    Date start = new Date();
	    try {
	      System.out.println("Indexing to directory '" + indexPath + "'...");
	      ObjectMapper mapper = new ObjectMapper();
		  HashMap<String, Integer> pagetable = mapper.readValue(new File(ptPath), HashMap.class);

	      Directory dir = FSDirectory.open(Paths.get(indexPath));
	      Analyzer analyzer = new WikiEngAnalyzer(usedStopWords, stemmer);
	      IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
	      iwc.setSimilarity(new CLESASimilarity());
	      iwc.setRAMBufferSizeMB(512.0);
	      iwc.setOpenMode(OpenMode.CREATE);

	      IndexWriter writer = new IndexWriter(dir, iwc);
	      int j = 0;
	      int size = pagetable.size();
	      for(String key : pagetable.keySet()){
	    	  try{
		    	  Path file = new File(docsPath + key).toPath();
		    	  j++;
		    	  System.out.println(j + "/" + size + ": " + key);
		    	  try (InputStream stream = Files.newInputStream(file)) {

		    	      Document doc = new Document();
		    	      
		    	      Field pathField = new StringField("path", file.toString(), Field.Store.YES);
		    	      doc.add(pathField);
	
		    	      try {
		    		      org.jsoup.nodes.Document docu = org.jsoup.Jsoup.parse(stream, "UTF-8", "http://example.com/");
		    		      org.jsoup.nodes.Element contentDiv = docu.select("div[id=content]").first();
		    		      contentDiv.select("table[id=toc]").forEach(e -> e.remove());
		    		      contentDiv.select("span[class=editsection]").forEach(e -> e.remove());
		    			  contentDiv.select("div[id=catlinks]").forEach(e -> e.remove());
		    			  
		    			  TokenStream stream2 = writer.getAnalyzer().tokenStream(null, new StringReader(contentDiv.text()));
		    			  stream2.reset();
		    			  int i = 0;
		    			  while (stream2.incrementToken()) {
		    				  i++;
		    				  if(i >= 100) break;
		    			  }
		    			  stream2.end();
		    			  stream2.close();
		    			  if(i < 100) continue;
		    		      doc.add(new TextField("contents", new BufferedReader(new StringReader(contentDiv.text()))));

		    	      } catch(Exception e){
		    	    	  //e.printStackTrace();
		    	    	  e.printStackTrace(ClefIndexing.logpw);
		    	    	  ClefIndexing.logpw.flush();
		    	    	  continue;
		    	      }
		    	      
		    	      if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
		    	        System.out.println("adding file");
		    	        writer.addDocument(doc);
		    	      }
		    	    } catch(Exception e){
						//e.printStackTrace();
						e.printStackTrace(logpw);
						ClefIndexing.logpw.flush();
		    	    }
	    	  } catch(Exception e){
	    		  //e.printStackTrace();
	    		  e.printStackTrace(logpw);
    	    	  ClefIndexing.logpw.flush();
	    	  }
	      }
	      
	      writer.close();

	      Date end = new Date();
	      System.out.println(end.getTime() - start.getTime() + " total milliseconds");

	    } catch (IOException e) {
	      System.out.println(" caught a " + e.getClass() +
	       "\n with message: " + e.getMessage());
	    }
	  }
}

final class WikiEngAnalyzer extends StopwordAnalyzerBase {

  public static final CharArraySet ENGLISH_STOP_WORDS_SET;
  
  static {
    final List<String> stopWords = Arrays.asList(
      "a", "an", "and", "are", "as", "at", "be", "but", "by",
      "for", "if", "in", "into", "is", "it",
      "no", "not", "of", "on", "or", "such",
      "that", "the", "their", "then", "there", "these",
      "they", "this", "to", "was", "will", "with"
    );
    final CharArraySet stopSet = new CharArraySet(stopWords, false);
    ENGLISH_STOP_WORDS_SET = CharArraySet.unmodifiableSet(stopSet); 
  }
  
  public static final int DEFAULT_MAX_TOKEN_LENGTH = 255;

  private int maxTokenLength = DEFAULT_MAX_TOKEN_LENGTH;
  private SnowballProgram stemmer;

  public static final CharArraySet STOP_WORDS_SET = ENGLISH_STOP_WORDS_SET;

  /** Builds an analyzer with the given stop words.
   * @param stopWords stop words */
  public WikiEngAnalyzer(CharArraySet stopWords, SnowballProgram stemmer) {
    super(stopWords);
    this.stemmer = stemmer;
  }

  /** Builds an analyzer with the default stop words ({@link #STOP_WORDS_SET}).
   */

  /**
   * Set the max allowed token length.  Tokens larger than this will be chopped
   * up at this token length and emitted as multiple tokens.  If you need to
   * skip such large tokens, you could increase this max length, and then
   * use {@code LengthFilter} to remove long tokens.  The default is
   * {@link StandardAnalyzer#DEFAULT_MAX_TOKEN_LENGTH}.
   */
  public void setMaxTokenLength(int length) {
    maxTokenLength = length;
  }
    
  /** Returns the current maximum token length
   * 
   *  @see #setMaxTokenLength */
  public int getMaxTokenLength() {
    return maxTokenLength;
  }

  @Override
  protected TokenStreamComponents createComponents(final String fieldName) {
    final StandardTokenizer src = new StandardTokenizer();
    src.setMaxTokenLength(maxTokenLength);
    TokenStream tok = new StandardFilter(src);
    tok = new LowerCaseFilter(tok);
    tok = new StopFilter(tok, stopwords);
    tok = new SnowballFilter(tok, stemmer);
    return new TokenStreamComponents(src, tok) {
      @Override
      protected void setReader(final Reader reader) {
        // So that if maxTokenLength was changed, the change takes
        // effect next time tokenStream is called:
        src.setMaxTokenLength(WikiEngAnalyzer.this.maxTokenLength);
        super.setReader(reader);
      }
    };
  }

  @Override
  protected TokenStream normalize(String fieldName, TokenStream in) {
    TokenStream result = new StandardFilter(in);
    result = new LowerCaseFilter(result);
    return result;
  }
}
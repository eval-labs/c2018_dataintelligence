package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.stream.Stream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/***
 * This class prints out statistics about the TREC dataset used for this run.
 * @author Marco Jungwirth
 *
 */

public class DatasetStats {

	public static void main(String[] args) throws IOException {
		String usage = "Usage:\tjava -jar DatasetStats.jar -dataset dir";
	    if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
	      System.out.println(usage);
	      System.exit(0);
	    }
	    
	    String datasetdir = "D:\\Downloads\\telonb\\German";
	    
	    for(int i = 0;i < args.length;i++) {
	      if ("-dataset".equals(args[i])) {
	    	  datasetdir = args[i+1];
	    	  i++;
	      }
	    }
		try {
	        SAXParserFactory factory = SAXParserFactory.newInstance();
	        SAXParser saxParser = factory.newSAXParser();
	        RecordParser recordhandler = new RecordParser();
			try (Stream<Path> paths = Files.walk(Paths.get(datasetdir))) {
			    paths.filter(Files::isRegularFile)
			        .forEach(x -> {
			        	System.out.println(".");
			        	try {
							saxParser.parse(x.toFile(), recordhandler);
						} catch (Exception e) {
							e.printStackTrace();
						}
			        });
			}
	        
	        for(Record la : recordhandler.records)
	        		System.out.println(la);
	        
	        System.out.println(recordhandler.records.size());
	    } catch (Exception e) {
	       e.printStackTrace();
	    }
	}

}

class RecordHandler extends DefaultHandler {
	
	HashSet<String> langs = new HashSet<String>();
   int record = 0;
   int gercount = 0;
   int frecount = 0;
   int engcount = 0;
   int otherlangcount = 0;
   boolean language = false;
   boolean title = false;
   boolean languagewastrue = false;
   int langcount = 0;
   int nolangcount = 0;
   int neitherlangnoretit = 0;
   int titlecount = 0;
   int notitlecount = 0;

   @Override
   public void startElement(String uri, 
   String localName, String qName, Attributes attributes) throws SAXException {

      if (qName.equalsIgnoreCase("record")) {
         record++;
      } else if (qName.equalsIgnoreCase("dc:title")) {
         titlecount++;
         title = true;
      } else if (qName.equalsIgnoreCase("dc:language")) {
    	  language = true;
    	  languagewastrue = true;
    	  langcount++;
      }
   }

   @Override
   public void endElement(String uri, 
   String localName, String qName) throws SAXException {
      if (qName.equalsIgnoreCase("record")) {
    	 if(!title && !languagewastrue)
    		 neitherlangnoretit++;
    	  
         if(title)
        	 title = false;
         else
        	 notitlecount++;
         
         if(languagewastrue)
        	 languagewastrue = false;
         else
        	 nolangcount++;
        	 
      }
   }

   @Override
   public void characters(char ch[], int start, int length) throws SAXException {
      
      if (language) {
    	  String langua = new String(ch, start, length);
    	  langs.add(langua);
		 switch(langua){
		 	case "ger": 
		 	case "GER": 
		 		gercount++; break;
			case "eng": 
			case "engl": 
			case "engl.": 
				engcount++; break;
			case "fre": 
				frecount++; break;
		 	default: otherlangcount++;
		 		
		 }
		 language = false;
      }
   }
}
package main;



/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.StopwordAnalyzerBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Analyzer.TokenStreamComponents;
import org.apache.lucene.analysis.de.GermanStemmer;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.index.TermContext;
import org.apache.lucene.queries.function.FunctionScoreQuery;
import org.apache.lucene.queries.function.valuesource.DocFreqValueSource;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.SmallFloat;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;
import org.tartarus.snowball.ext.FrenchStemmer;
import org.tartarus.snowball.ext.German2Stemmer;

/**
 * This class is mainly used to test the CLESASimilarity implementation defined below.
 * @author Marco Jungwirth
 *
 */
public class SearchFiles {

  private SearchFiles() {}

  /** Simple command-line based search demo. */
  public static void main(String[] args) throws Exception {
    String usage =
      "Usage:\tjava org.apache.lucene.demo.SearchFiles [-index dir] [-field f] [-repeat n] [-queries file] [-query string] [-raw] [-paging hitsPerPage]\n\nSee http://lucene.apache.org/core/4_1_0/demo/ for details.";
    if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
      System.out.println(usage);
      System.exit(0);
    }

    String index = "index";
    String field = "contents";
    String queries = null;
    int repeat = 0;
    boolean raw = false;
    String queryString = null;
    int hitsPerPage = 10;
    
    for(int i = 0;i < args.length;i++) {
      if ("-index".equals(args[i])) {
        index = args[i+1];
        i++;
      } else if ("-field".equals(args[i])) {
        field = args[i+1];
        i++;
      } else if ("-queries".equals(args[i])) {
        queries = args[i+1];
        i++;
      } else if ("-query".equals(args[i])) {
        queryString = args[i+1];
        i++;
      } else if ("-repeat".equals(args[i])) {
        repeat = Integer.parseInt(args[i+1]);
        i++;
      } else if ("-raw".equals(args[i])) {
        raw = true;
      } else if ("-paging".equals(args[i])) {
        hitsPerPage = Integer.parseInt(args[i+1]);
        if (hitsPerPage <= 0) {
          System.err.println("There must be at least 1 hit per page.");
          System.exit(1);
        }
        i++;
      }
    }
    
    IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
    IndexSearcher searcher = new IndexSearcher(reader);
    //searcher.setSimilarity(new BM25Similarity());
    searcher.setSimilarity(new CLESASimilarity());
    Analyzer analyzer = new WikiEngAnalyzer(ClefIndexing.stopSetGer, new German2Stemmer());
    //Analyzer analyzer = new StandardAnalyzer();

    BufferedReader in = null;
    if (queries != null) {
      in = Files.newBufferedReader(Paths.get(queries), StandardCharsets.UTF_8);
    } else {
      in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
    }
    QueryParser parser = new QueryParser(field, analyzer);
    while (true) {
      if (queries == null && queryString == null) {                        // prompt the user
        System.out.println("Enter query: ");
      }

      String line = queryString != null ? queryString : in.readLine();

      if (line == null || line.length() == -1) {
        break;
      }

      line = line.trim();
      if (line.length() == 0) {
        break;
      }
      
      Query query = parser.parse(line);
      BooleanQuery bq = (BooleanQuery) query;
      bq.clauses().forEach(x -> {
    	  TermQuery tq = (TermQuery) x.getQuery();
    	  
    	  	try {
    	  		System.out.println(tq.getTerm().text() + ": " + TermContext.build(reader.getContext(), tq.getTerm()).docFreq());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
      });
      //DocFreqValueSource dfvs = new DocFreqValueSource(field, val, indexedField, indexedBytes)
      //FunctionScoreQuery.boostByValue(query, )
      
      System.out.println("Searching for: " + query.toString(field));
            
      if (repeat > 0) {                           // repeat & time as benchmark
        Date start = new Date();
        for (int i = 0; i < repeat; i++) {
          searcher.search(query, 100);
        }
        Date end = new Date();
        System.out.println("Time: "+(end.getTime()-start.getTime())+"ms");
      }

      doPagingSearch(in, searcher, query, hitsPerPage, raw, queries == null && queryString == null);

      if (queryString != null) {
        break;
      }
    }
    reader.close();
  }

  /**
   * This demonstrates a typical paging search scenario, where the search engine presents 
   * pages of size n to the user. The user can then go to the next page if interested in
   * the next hits.
   * 
   * When the query is executed for the first time, then only enough results are collected
   * to fill 5 result pages. If the user wants to page beyond this limit, then the query
   * is executed another time and all hits are collected.
   * 
   */
  public static void doPagingSearch(BufferedReader in, IndexSearcher searcher, Query query, 
                                     int hitsPerPage, boolean raw, boolean interactive) throws IOException {
 
    // Collect enough docs to show 5 pages                                                                    
    TopDocs results = searcher.search(query, 5 * hitsPerPage);
    ScoreDoc[] hits = results.scoreDocs;
    
    int numTotalHits = Math.toIntExact(results.totalHits);
    System.out.println(numTotalHits + " total matching documents");

    int start = 0;
    int end = Math.min(numTotalHits, hitsPerPage);
        
    while (true) {
      if (end > hits.length) {
        System.out.println("Only results 1 - " + hits.length +" of " + numTotalHits + " total matching documents collected.");
        System.out.println("Collect more (y/n) ?");
        String line = in.readLine();
        if (line.length() == 0 || line.charAt(0) == 'n') {
          break;
        }

        hits = searcher.search(query, numTotalHits).scoreDocs;
      }
      
      end = Math.min(hits.length, start + hitsPerPage);
      
      for (int i = start; i < end; i++) {
        if (raw) {                              // output raw format
          System.out.println("   Title: " + searcher.doc(hits[i].doc).get("path"));
          System.out.println("doc="+hits[i].doc+" score="+hits[i].score + ", actual score=" + hits[i].score);
          System.out.println(searcher.explain(query, hits[i].doc));
          continue;
        }

        Document doc = searcher.doc(hits[i].doc);
        String path = doc.get("path");
        if (path != null) {
          System.out.println((i+1) + ". " + path);
          String title = doc.get("title");
          if (title != null) {
            System.out.println("   Title: " + doc.get("title"));
          }
        } else {
          System.out.println((i+1) + ". " + "No path for this document");
        }
                  
      }

      if (!interactive || end == 0) {
        break;
      }

      if (numTotalHits >= end) {
        boolean quit = false;
        while (true) {
          System.out.print("Press ");
          if (start - hitsPerPage >= 0) {
            System.out.print("(p)revious page, ");  
          }
          if (start + hitsPerPage < numTotalHits) {
            System.out.print("(n)ext page, ");
          }
          System.out.println("(q)uit or enter number to jump to a page.");
          
          String line = in.readLine();
          if (line.length() == 0 || line.charAt(0)=='q') {
            quit = true;
            break;
          }
          if (line.charAt(0) == 'p') {
            start = Math.max(0, start - hitsPerPage);
            break;
          } else if (line.charAt(0) == 'n') {
            if (start + hitsPerPage < numTotalHits) {
              start+=hitsPerPage;
            }
            break;
          } else {
            int page = Integer.parseInt(line);
            if ((page - 1) * hitsPerPage < numTotalHits) {
              start = (page - 1) * hitsPerPage;
              break;
            } else {
              System.out.println("No such page");
            }
          }
        }
        if (quit) break;
        end = Math.min(numTotalHits, start + hitsPerPage);
      }
    }
  }
}



class CLESASimilarity extends Similarity {

	  
	  /** BM25 with these default values:
	   * <ul>
	   *   <li>{@code k1 = 1.2}</li>
	   *   <li>{@code b = 0.75}</li>
	   * </ul>
	   */
	  public CLESASimilarity() {
	  }
	  
	  protected float idf(long docFreq, long docCount) {
	    return (float) (1 + Math.log((docCount + 1D)/docFreq));
	  }
	  

	  /** The default implementation returns <code>1</code> */
	  protected float scorePayload(int doc, int start, int end, BytesRef payload) {
	    return 1;
	  }
	  
	  /** 
	   * True if overlap tokens (tokens with a position of increment of zero) are
	   * discounted from the document's length.
	   */
	  protected boolean discountOverlaps = false;

	  /** Sets whether overlap tokens (Tokens with 0 position increment) are 
	   *  ignored when computing norm.  By default this is true, meaning overlap
	   *  tokens do not count when computing norms. */
	  public void setDiscountOverlaps(boolean v) {
	    discountOverlaps = v;
	  }
	  
	  /**
	   * Returns true if overlap tokens are discounted from the document's length. 
	   * @see #setDiscountOverlaps 
	   */
	  public boolean getDiscountOverlaps() {
	    return discountOverlaps;
	  }
	  
	  /** Cache of decoded bytes. */
	  private static final float[] OLD_LENGTH_TABLE = new float[256];
	  private static final float[] LENGTH_TABLE = new float[256];

	  static {
	    for (int i = 1; i < 256; i++) {
	      float f = SmallFloat.byte315ToFloat((byte)i);
	      OLD_LENGTH_TABLE[i] = 1.0f / (f*f);
	    }
	    OLD_LENGTH_TABLE[0] = 1.0f / OLD_LENGTH_TABLE[255]; // otherwise inf

	    for (int i = 0; i < 256; i++) {
	      LENGTH_TABLE[i] = SmallFloat.byte4ToInt((byte) i);
	    }
	  }


	  @Override
	  public final long computeNorm(FieldInvertState state) {
	    final int numTerms = discountOverlaps ? state.getLength() - state.getNumOverlap() : state.getLength();
	    int indexCreatedVersionMajor = state.getIndexCreatedVersionMajor();
	    if (indexCreatedVersionMajor >= 7) {
	      return SmallFloat.intToByte4(numTerms);
	    } else {
	      return SmallFloat.floatToByte315((float) (1 / Math.sqrt(numTerms)));
	    }
	  }

	  /**
	   * Computes a score factor for a simple term and returns an explanation
	   * for that score factor.
	   * 
	   * <p>
	   * The default implementation uses:
	   * 
	   * <pre class="prettyprint">
	   * idf(docFreq, docCount);
	   * </pre>
	   * 
	   * Note that {@link CollectionStatistics#docCount()} is used instead of
	   * {@link org.apache.lucene.index.IndexReader#numDocs() IndexReader#numDocs()} because also 
	   * {@link TermStatistics#docFreq()} is used, and when the latter 
	   * is inaccurate, so is {@link CollectionStatistics#docCount()}, and in the same direction.
	   * In addition, {@link CollectionStatistics#docCount()} does not skew when fields are sparse.
	   *   
	   * @param collectionStats collection-level statistics
	   * @param termStats term-level statistics for the term
	   * @return an Explain object that includes both an idf score factor 
	             and an explanation for the term.
	   */
	  public Explanation idfExplain(CollectionStatistics collectionStats, TermStatistics termStats) {
	    final long df = termStats.docFreq();
	    final long docCount = collectionStats.docCount() == -1 ? collectionStats.maxDoc() : collectionStats.docCount();
	    final float idf = idf(df, docCount);
	    return Explanation.match(idf, "idf, computed as 1 + log( docfreq / (docCount + 1)) from:",
	        Explanation.match(df, "docFreq"),
	        Explanation.match(docCount, "docCount"));
	  }

	  /**
	   * Computes a score factor for a phrase.
	   * 
	   * <p>
	   * The default implementation sums the idf factor for
	   * each term in the phrase.
	   * 
	   * @param collectionStats collection-level statistics
	   * @param termStats term-level statistics for the terms in the phrase
	   * @return an Explain object that includes both an idf 
	   *         score factor for the phrase and an explanation 
	   *         for each term.
	   */
	  public Explanation idfExplain(CollectionStatistics collectionStats, TermStatistics termStats[]) {
	    double idf = 0d; // sum into a double before casting into a float
	    List<Explanation> details = new ArrayList<>();
	    for (final TermStatistics stat : termStats ) {
	      Explanation idfExplain = idfExplain(collectionStats, stat);
	      details.add(idfExplain); // Todo get term frequency
	      idf += idfExplain.getValue();
	    }
	    return Explanation.match((float) idf, "idf(), sum of:", details);
	  }

	  @Override
	  public final SimWeight computeWeight(float boost, CollectionStatistics collectionStats, TermStatistics... termStats) {
	    Explanation idf = termStats.length == 1 ? idfExplain(collectionStats, termStats[0]) : idfExplain(collectionStats, termStats);

	    float[] oldCache = new float[256];
	    float[] cache = new float[256];
	    for (int i = 0; i < cache.length; i++) {
	      oldCache[i] = OLD_LENGTH_TABLE[i];
	      cache[i] = LENGTH_TABLE[i];
	    }
	    return new CLESAStats(collectionStats.field(), idf, oldCache, cache);
	  }

	  @Override
	  public final SimScorer simScorer(SimWeight stats, LeafReaderContext context) throws IOException {
		  CLESAStats clesastats = (CLESAStats) stats;
	    return new CLESAScorer(clesastats, context.reader().getMetaData().getCreatedVersionMajor(), context.reader().getNormValues(clesastats.field));
	   // return new CLESAScorer(clesastats, context.reader().getNormValues(clesastats.field));
	  }
	  
	  private class CLESAScorer extends SimScorer {
	    private final CLESAStats stats;
	    private final float weightValue; // boost * idf * (k1 + 1)
	    private final NumericDocValues norms;
	    /** precomputed cache for all length values */
	    private final float[] lengthCache;
	    /** precomputed norm[256] with k1 * ((1 - b) + b * dl / avgdl) */
	    private final float[] cache;
	    
	    CLESAScorer(CLESAStats stats, int indexCreatedVersionMajor, NumericDocValues norms) throws IOException {
	    //CLESAScorer(CLESAStats stats, NumericDocValues norms) throws IOException {
	      this.stats = stats;
	      this.weightValue = stats.weight;
	      this.norms = norms;
	      if (indexCreatedVersionMajor >= 7) {
	        lengthCache = LENGTH_TABLE;
	        cache = stats.cache;
	      } else {
	        lengthCache = OLD_LENGTH_TABLE;
	        cache = stats.oldCache;
	      }
	    }
	    
	    @Override
	    public float score(int doc, float freq) throws IOException {
	      // if there are no norms, we act as if b=0
	      float norm;
	      if (norms == null) {
	        norm = 0;
	      } else {
	        if (norms.advanceExact(doc)) {
	          norm = cache[((byte) norms.longValue()) & 0xFF];
	        } else {
	          norm = cache[0];
	        }
	      }
	      return (float) (weightValue * Math.sqrt(freq) * (1 / Math.sqrt(norm)));
	    }
	    
	    @Override
	    public Explanation explain(int doc, Explanation freq) throws IOException {
	      return explainScore(doc, freq, stats, norms, lengthCache);
	    }

	    @Override
	    public float computeSlopFactor(int distance) {
	      return  1.0f / (distance + 1);
	    }

	    @Override
	    public float computePayloadFactor(int doc, int start, int end, BytesRef payload) {
	      return scorePayload(doc, start, end, payload);
	    }
	  }
	  
	  /** Collection statistics for the BM25 model. */
	  private static class CLESAStats extends SimWeight {
	    /** BM25's idf */
	    private final Explanation idf;
	    /** weight (idf * boost) */
	    private final float weight;
	    /** field name, for pulling norms */
	    private final String field;
	    /** precomputed norm[256] with k1 * ((1 - b) + b * dl / avgdl)
	     *  for both OLD_LENGTH_TABLE and LENGTH_TABLE */
	    private final float[] oldCache, cache;

	    CLESAStats(String field, Explanation idf, float[] oldCache, float[] cache) {
	      this.field = field;
	      this.idf = idf;
	      this.weight = idf.getValue();
	      this.oldCache = oldCache;
	      this.cache = cache;
	      
	    }

	  }

	  private Explanation explainTFNorm(int doc, Explanation freq, CLESAStats stats, NumericDocValues norms, float[] lengthCache) throws IOException {
	    List<Explanation> subs = new ArrayList<>();
	    subs.add(freq);
	    if (norms == null) {
	      subs.add(Explanation.match(0, "parameter b (norms omitted for field)"));
	      return Explanation.match(
	          (freq.getValue() ) / (freq.getValue()),
	          "tfNorm, computed as (freq * (k1 + 1)) / (freq + k1) from:", subs);
	    } else {
	      byte norm;
	      if (norms.advanceExact(doc)) {
	        norm = (byte) norms.longValue();
	      } else {
	        norm = 0;
	      }
	      float doclen = (float) (1 / Math.sqrt(lengthCache[norm & 0xff]));
	      subs.add(Explanation.match(doclen, "fieldLength"));
	      return Explanation.match(
	          (float) (Math.sqrt(freq.getValue()) * doclen) ,
	          "tfNorm, computed as (freq * fieldLength) from:", subs);
	    }
	  }

	  private Explanation explainScore(int doc, Explanation freq, CLESAStats stats, NumericDocValues norms, float[] lengthCache) throws IOException {
	    List<Explanation> subs = new ArrayList<>();

	    subs.add(stats.idf);
	    Explanation tfNormExpl = explainTFNorm(doc, freq, stats, norms, lengthCache);
	    subs.add(tfNormExpl);
	    return Explanation.match(
	        stats.idf.getValue() * tfNormExpl.getValue(),
	        "score(doc="+doc+",freq="+freq+"), product of:", subs);
	  }

	  @Override
	  public String toString() {
	    return "CLESA";
	  }

}


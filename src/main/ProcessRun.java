package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermContext;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.tartarus.snowball.ext.EnglishStemmer;
import org.tartarus.snowball.ext.FrenchStemmer;
import org.tartarus.snowball.ext.GermanStemmer;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is used to produce results from all the componentes coming before. Various helper classes are defined below to parse topics and
 * records and to print the results in the right forma into a file.
 * @author Marco Jungwirth
 *
 */
public class ProcessRun {

	public static void main(String[] args) throws IOException, ParseException {
		String usage =
	      "Usage:\tjava -jar ProcessRun.jar -enindex dir -deindex dir -frindex dir -topic file -dataset dir -output file -clmfretoeng file -clmgertoeng file -topick num -reck num -log file";
	    if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
	      System.out.println(usage);
	      System.exit(0);
	    }
	    
	    String deindex = "index", enindex = "index", frindex = "index";
	    String topicfile = "";
	    String datasetdir = "";
	    String outputfile = "";
	    String clmfretoeng = "";
	    String clmgertoeng = "";
	    String logfile = "D:/clef/logs/experimentlog.txt";
	    Integer topicDocs = 10000;
	    Integer recordDocs = 1000;
	    
	    for(int i = 0;i < args.length;i++) {
	      if ("-enindex".equals(args[i])) {
	    	enindex = args[i+1];
	        i++;
	      } else if ("-deindex".equals(args[i])) {
	    	deindex = args[i+1];
	        i++;
	      }  else if ("-frindex".equals(args[i])) {
	    	frindex = args[i+1];
	        i++;
	      } else if ("-topic".equals(args[i])) {
	    	topicfile = args[i+1];
	        i++;
	      } else if ("-dataset".equals(args[i])) {
	    	datasetdir = args[i+1];
	        i++;
	      } else if ("-output".equals(args[i])) {
	    	outputfile = args[i+1];
	        i++;
	      } else if ("-clmfretoeng".equals(args[i])) {
	    	clmfretoeng = args[i+1];
	        i++;
	      } else if ("-clmgertoeng".equals(args[i])) {
	    	clmgertoeng = args[i+1];
	        i++;
	      } else if ("-topick".equals(args[i])) {
	    	topicDocs = Integer.parseInt(args[i+1]);
	        i++;
	      } else if ("-reck".equals(args[i])) {
	    	recordDocs = Integer.parseInt(args[i+1]);
	        i++;
	      } else if ("-log".equals(args[i])) {
	    	recordDocs = Integer.parseInt(args[i+1]);
	        i++;
	      }
	    }
	    PrintWriter logw = new PrintWriter(new File(logfile));
	    List<Topic> topics = parseTopics(topicfile);
	    for(Topic t : topics){
	    	System.out.println(t.toString());
	    }

	    IndexReader enreader = DirectoryReader.open(FSDirectory.open(Paths.get(enindex)));
	    IndexSearcher ensearcher = new IndexSearcher(enreader);
	    ensearcher.setSimilarity(new CLESASimilarity());
	    
	    IndexReader dereader = DirectoryReader.open(FSDirectory.open(Paths.get(deindex)));
	    IndexSearcher desearcher = new IndexSearcher(dereader);
	    desearcher.setSimilarity(new CLESASimilarity());
	    
	    IndexReader frreader = DirectoryReader.open(FSDirectory.open(Paths.get(frindex)));
	    IndexSearcher frsearcher = new IndexSearcher(frreader);
	    frsearcher.setSimilarity(new CLESASimilarity());
	    
	    Analyzer enanalyzer = new WikiEngAnalyzer(ClefIndexing.stopSetEng, new EnglishStemmer());
	    Analyzer deanalyzer = new WikiEngAnalyzer(ClefIndexing.stopSetGer, new GermanStemmer());
	    Analyzer franalyzer = new WikiEngAnalyzer(ClefIndexing.stopSetFre, new FrenchStemmer());
	    
	    QueryParser enparser = new QueryParser("contents", enanalyzer);
	    QueryParser deparser = new QueryParser("contents", deanalyzer);
	    QueryParser frparser = new QueryParser("contents", franalyzer);
	    
	    int enesalength = enreader.maxDoc();
	    int deesalength = dereader.maxDoc();
	    int fresalength = frreader.maxDoc();
	    
	    System.out.println(enesalength + " - " + deesalength + " - " + fresalength);

		ObjectMapper mapper = new ObjectMapper();
	    HashMap<String, List<Integer>> fretoengmap = mapper.readValue(new File(clmfretoeng), HashMap.class);
	    HashMap<String, List<Integer>> gertoengmap = mapper.readValue(new File(clmgertoeng), HashMap.class);
	    
	    HashMap<Integer, String> fretoenglookup = genLookupTable(fretoengmap);
	    HashMap<Integer, String> gertoenglookup = genLookupTable(gertoengmap);

	    int topiccount = 0;
	    
	    for(Topic t : topics){
	    	topiccount++;
	        System.out.println(topiccount + "/50");
	    	Query query = enparser.parse(t.getTitle());
			double ctfactor = calculateCTFactor(query, enreader, enesalength);
	        ScoreDoc[] hits = ensearcher.search(query, topicDocs).scoreDocs;
	        HashMap<Integer, Float> esavector = new HashMap<Integer, Float>();
	        for(ScoreDoc sd : hits){
	        	esavector.put(sd.doc, new Float(sd.score/ctfactor));
	        }
	        t.setEsavec(esavector);
	    }
	    
	    List<Record> records = parseDataset(datasetdir);
	    
	    int reccount = 0;
	    for(Record rec : records){
	    	reccount++;
	    	System.out.println(reccount + "/" + records.size() + " Time: " + LocalTime.now());
	    	
	    	if(rec.getTitle().trim().isEmpty())
	    		continue;
	    	
	    	ScoreDoc[] hits;
	    	
        	boolean fretoeng = false;
        	boolean gertoeng = false;
        	//double ctfactor = 0.0;
        	try{
				switch(rec.getLanguage()){
					case "fre":
						Query qfre = frparser.parse(rec.getTitle());
						ctfactor = calculateCTFactor(qfre, frreader, fresalength);
						hits = frsearcher.search(qfre, recordDocs).scoreDocs;
						fretoeng = true;
						break;
					case "eng":
						Query qeng = enparser.parse(rec.getTitle());
						ctfactor = calculateCTFactor(qeng, enreader, enesalength);
						hits = ensearcher.search(qeng, recordDocs).scoreDocs;
						gertoeng = true;
						break;
					/*case "":
						ScoreDoc[] hits1 = frsearcher.search(frparser.parse(title), fresalength).scoreDocs;
						ScoreDoc[] hits2 = desearcher.search(deparser.parse(title), deesalength).scoreDocs;
						ScoreDoc[] hits3 = ensearcher.search(enparser.parse(title), enesalength).scoreDocs;
						if(hits1.length > hits2.length && hits1.length > hits3.length){
							hits = frsearcher.search(frparser.parse(title), 1000).scoreDocs;;
							break;
						}
						
						if(hits2.length > hits1.length && hits2.length > hits3.length){
							hits = desearcher.search(deparser.parse(title), 1000).scoreDocs;
							gertoeng = true;
							break;
						}
						
						if(hits3.length > hits1.length && hits3.length > hits2.length){
							hits = ensearcher.search(enparser.parse(title), 1000).scoreDocs;
							fretoeng = true;
							break;
						}*/
						
					default:
						Query qger = deparser.parse(rec.getTitle());
						ctfactor = calculateCTFactor(qger, dereader, deesalength);
						hits = desearcher.search(qger, recordDocs).scoreDocs;
				}
        	}catch(Exception e){
        		e.printStackTrace();
        		e.printStackTrace(logw);
        		logw.flush();
        		continue;
        	}
			
			if(hits.length == 0){
				continue;
			}
			
        	HashMap<Integer,Float> recordEsaVec = new HashMap<Integer, Float>();
			
			for(int i = 0; i < hits.length; i++){
				recordEsaVec.put(hits[i].doc, new Float(hits[i].score*ctfactor));
			}
			
			if(fretoeng){
				recordEsaVec = transformEsaVector(recordEsaVec, fretoengmap, fretoenglookup);
			}
			
			if(gertoeng){
				recordEsaVec = transformEsaVector(recordEsaVec, gertoengmap, gertoenglookup);
			}
			
			if(recordEsaVec.size() == 0){
				continue;
			}
        	
        	for(Topic t : topics){
        		Double score = new Double(cosineDistance(t.getEsavec(), recordEsaVec));
        		if(score != 0 && !score.isNaN())
        			t.getRetdocs().add(new TrecResult(rec.getId(), score));
        	}
	    	
	    }
		
	    PrintWriter pw = new PrintWriter(new File(outputfile));
	    for(Topic t : topics){
	    	pw.print(t.toTrecOutput());
	    }
	    pw.flush();
	    pw.close();
	    logw.flush();
	    logw.close();
	    enreader.close();
	    frreader.close();
	    dereader.close();
	}
	
	private static float calculateIDF(long docFreq, long docCount){
		return (float) (1 + Math.log((docCount + 1D)/docFreq));
	}
	
	private static double calculateCTFactor(Query query, IndexReader reader, long docCount) throws IOException {
		double result = 0.0;
		if(query instanceof BooleanQuery) {
			BooleanQuery bq = (BooleanQuery) query;
			for(BooleanClause bc : bq.clauses()) {
				TermQuery tq = (TermQuery) bc.getQuery();
			  	result += calculateIDF(TermContext.build(reader.getContext(), tq.getTerm()).docFreq(), docCount);
			}
		} else if(query instanceof TermQuery) {
			TermQuery tq = (TermQuery) query;
			result += calculateIDF(TermContext.build(reader.getContext(), tq.getTerm()).docFreq(), docCount);
		}
		return 1/Math.sqrt(result);
	}
	
	private static HashMap<Integer, String> genLookupTable(HashMap<String, List<Integer>> map){
		HashMap<Integer, String> result = new HashMap<Integer, String>();
		for(String key : map.keySet()){
			List<Integer> lst = map.get(key);
			for(Integer i : lst){
				result.put(i, key);
			}
		}
		return result;
	}
	
	private static HashMap<Integer, Float> transformEsaVector(HashMap<Integer, Float> source, HashMap<String, List<Integer>> map, HashMap<Integer, String> lookuptable){
		HashMap<Integer, Float>  result = new HashMap<Integer, Float>();
		for(Integer key : source.keySet()){
			if(!lookuptable.containsKey(key)) continue;
			String mapkey = lookuptable.get(key);
			Integer intid = Integer.parseInt(mapkey);
			float sum = 0;
			for(Integer i : map.get(mapkey)){
				if(source.containsKey(i))
					sum += source.get(i);
			}
			result.put(intid, sum);
		}
		return result;
	}
	
	private static double cosineDistance(HashMap<Integer, Float> a, HashMap<Integer, Float> b){
		double cosdist = 0;
		double asquare = 0, bsquare = 0;
		
		for(Integer key : a.keySet()){
			float val = a.get(key);
			asquare += val * val;
			if(b.containsKey(key)) 
				cosdist += val * b.get(key);
		}
		
		for(Integer key : b.keySet()){
			float val = b.get(key);
			bsquare += val * val;
		}
		
		return cosdist/(Math.sqrt(asquare) * Math.sqrt(bsquare));
	}
	
	private static List<Record> parseDataset(String datasetdir) throws IOException {
		List<Record> result = null;
		try {
	        SAXParserFactory factory = SAXParserFactory.newInstance();
	        SAXParser saxParser = factory.newSAXParser();
	        RecordParser recordhandler = new RecordParser();
			try (Stream<Path> paths = Files.walk(Paths.get(datasetdir))) {
			    paths.filter(Files::isRegularFile)
			        .forEach(x -> {
			        	System.out.println(".");
			        	try {
							saxParser.parse(x.toFile(), recordhandler);
						} catch (Exception e) {
							e.printStackTrace();
						}
			        });
			}
	        
	        System.out.println(recordhandler.records.size());
	        result = recordhandler.records;
	    } catch (Exception e) {
	       e.printStackTrace();
	    }
		return result;
	}

	private static List<Topic> parseTopics(String topicfile) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(topicfile)));
		List<Topic> result = new ArrayList<Topic>();
		String line = "";
		while((line = reader.readLine()) != null){
			if(line.contains("<topic")){
				String id = "", title = "";
				for(int i = 0; i < 5; i++){
					String line2 = reader.readLine();
					if(line2.contains("<identifier>")){
						id = line2.substring(12, line2.length()-13);
					}
					if(line2.contains("<title>")){
						title = line2.substring(7, line2.length()-8);
					}
				}
				result.add(new Topic(id, title));
			}
			
		}
		reader.close();
		return result;
	}

}

class TrecResult{
	private String docId;
	private double score;
	
	public TrecResult() {
		super();
		this.docId = "";
		this.score = 0;
	}
	
	public TrecResult(String id, double score) {
		super();
		this.docId = id;
		this.score = score;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
}

class Record{
	private String id;
	private String title;
	private String language;
	
	public Record() {
		super();
		this.id = "";
		this.title = "";
		this.language = "";
	}
	
	public Record(String id, String title, String language) {
		super();
		this.id = id;
		this.title = title;
		this.language = language;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Record [id=" + id + ", title=" + title + ", language="
				+ language + "]";
	}
	
	
}

class RecordParser extends DefaultHandler {
	
	List<Record> records = new ArrayList<Record>();
	private static final Pattern UNWANTED_SYMBOLS = Pattern.compile("[^\\p{Alnum}&&[^\\p{Space}&&[^�������������������������Ԍ��ܟ��������]]]");
	
   boolean id = false;
   boolean language = false;
   boolean hastitle = false;
   boolean title = false;
   StringBuilder buildtitle = new StringBuilder();
   String recid = "";
   String reclanguage = "";
   String rectitle = "";

   @Override
   public void startElement(String uri, 
   String localName, String qName, Attributes attributes) throws SAXException {

      if (qName.equalsIgnoreCase("id")) {
         id = true;
      } else if (qName.equalsIgnoreCase("dc:title")) {
         title = true;
         hastitle = true;
      } else if (qName.equalsIgnoreCase("dc:language")) {
    	  language = true;
      }
   }

   @Override
   public void endElement(String uri, 
   String localName, String qName) throws SAXException {
      if (qName.equalsIgnoreCase("record") && hastitle) {
    	  Matcher unwantedMatcher = UNWANTED_SYMBOLS.matcher(buildtitle.toString());
    	  //System.out.println(buildtitle.toString() + " ------- " + unwantedMatcher.replaceAll(" "));
    	 records.add(new Record(recid, unwantedMatcher.replaceAll(" "), reclanguage));
    	 buildtitle.setLength(0);
         hastitle = false;
      } else if(qName.equalsIgnoreCase("dc:title")){
	      title = false;
      }
      
   }

   @Override
   public void characters(char ch[], int start, int length) throws SAXException {
      
      if (language) {
    	  String langua = new String(ch, start, length);
		 switch(langua){
		 	case "ger": 
		 	case "GER": 
		 		reclanguage = "ger"; break;
			case "eng": 
			case "engl": 
			case "engl.": 
				reclanguage = "eng"; break;
			case "fre": 
				reclanguage = "fre"; break;
		 	default: reclanguage = "ger"; break;
		 		
		 }
		 language = false;
      } else if (title) {
    	  buildtitle.append(new String(ch, start, length));
	  } else if (id) {
		  recid = new String(ch, start, length);
		  id = false;
	  }
   }
}

class Topic{
	private String id;
	private String title;
	private HashMap<Integer, Float> esavec;
	private List<TrecResult> retdocs;
	
	public Topic(String id, String title) {
		super();
		this.id = id;
		this.title = title;
		this.esavec = new HashMap<Integer, Float>();
		this.retdocs = new ArrayList<TrecResult>();
	}

	public Topic(String id, String title, HashMap<Integer, Float> esavec) {
		super();
		this.id = id;
		this.title = title;
		this.esavec = esavec;
		this.retdocs = new ArrayList<TrecResult>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public HashMap<Integer, Float> getEsavec() {
		return esavec;
	}

	public void setEsavec(HashMap<Integer, Float> esavec) {
		this.esavec = esavec;
	}

	public List<TrecResult> getRetdocs() {
		return retdocs;
	}

	public void setRetdocs(List<TrecResult> retdocs) {
		this.retdocs = retdocs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Topic other = (Topic) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public String toTrecOutput() {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(6);
		String result = "";
		
		retdocs.sort(new Comparator<TrecResult>(){

			@Override
			public int compare(TrecResult arg0, TrecResult arg1) {
				if(arg0.getScore() == arg1.getScore())
					return 0;
				if(arg0.getScore() > arg1.getScore())
					return -1;
				return 1;
			}
			
		});
		int i = 0;
		for(TrecResult tr : retdocs){
			if(i == 1000)
				break;
			i++;
			result += this.id.substring(0,3) + " Q0 " + tr.getDocId() + " " + i + " " + nf.format(tr.getScore()) + " dataintelligence_sorg2008_replic\n";
		}
		
		return result;
	}

	@Override
	public String toString() {
		return "Topic [id=" + id + ", title=" + title + "]";
	}
}
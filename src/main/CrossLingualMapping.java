package main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.Paths;
import java.security.Timestamp;
import java.time.Instant;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.print.attribute.standard.DateTimeAtCompleted;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.FSDirectory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The class CrossLinugalMapping is a little program which calculates the cross-lingual mapping from one wikipedia index to another wikipedia index
 * and it saves the the mapping as JSON file on disk. Document ids from the indices are used to connect the indices.
 * @author Marco Jungwirth
 *
 */

public class CrossLingualMapping {

	public static void main(String[] args) throws IOException, ParseException {
		String usage =
	      "Usage:\tjava -jar CrossLingualMapping.jar -indexfrom dir -indexto dir -output file -log file";
	    if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
	      System.out.println(usage);
	      System.exit(0);
	    }
	    String indexfrom = "D:/Downloads/wikipedia-de-html/index", indexto = "D:/Downloads/wikipedia-en-html/index";
	    String outputfile = "D:/clef/clmapping.json";
	    String logfile = "D:/clef/logs/cllogs.txt";
	    
	    for(int i = 0;i < args.length;i++) {
	      if ("-indexfrom".equals(args[i])) {
	    	  indexfrom = args[i+1];
	        i++;
	      } else if ("-indexto".equals(args[i])) {
	    	  indexto = args[i+1];
	        i++;
	      } else if ("-output".equals(args[i])) {
	    	outputfile = args[i+1];
	        i++;
	      } else if ("-log".equals(args[i])) {
	    	  logfile = args[i+1];
		        i++;
		      }
	    }
		PrintWriter pw = new PrintWriter(new File(logfile));
		HashMap<Integer, List<Integer>> clmapping = new HashMap<Integer, List<Integer>>();
		IndexReader readerfrom = DirectoryReader.open(FSDirectory.open(Paths.get(indexfrom)));
	    IndexSearcher searcherfrom = new IndexSearcher(readerfrom);
		IndexReader readerto = DirectoryReader.open(FSDirectory.open(Paths.get(indexto)));
	    IndexSearcher searcherto = new IndexSearcher(readerto);

	    QueryParser enparser = new QueryParser("path", new StandardAnalyzer());
	    int max = readerfrom.maxDoc();
	    for(int i = 0; i < max; i++){
	    	try{
		    	if(i % 1000 == 0) System.out.println(i + "/" + max + " Time: " + LocalTime.now());
		    	String path = readerfrom.document(i).getField("path").stringValue();
			    File input = new File(path);
				org.jsoup.nodes.Document doc = org.jsoup.Jsoup.parse(input, "UTF-8", "http://example.com/");
				org.jsoup.nodes.Element element = doc.selectFirst("div[id=p-lang]");
				
				if(element == null)
					continue;
				
				org.jsoup.select.Elements links = element.select("a[href]");
				for(org.jsoup.nodes.Element link : links){
					String langpath = java.net.URLDecoder.decode(link.attr("href"));
					if(langpath.contains("/en/")){
						String newpath = "D:\\Downloads\\wikipedia-en-html\\wikipedia-en-html\\en\\articles\\" + langpath.substring(27).replace("/", "\\");
						File f = new File(newpath);
						if(!f.exists()){
							String newpath2 = newpath.substring(0) + ".ign";
							f = new File(newpath2);
							if(!f.exists()){
								String newpath3 = newpath.substring(0, newpath.length()-4) + "ign";
								f = new File(newpath3);
								if(!f.exists()){
									continue;
								}
							}
							org.jsoup.nodes.Document redirect = org.jsoup.Jsoup.parse(f, "UTF-8", "http://example.com/");
							org.jsoup.nodes.Element linkelement = redirect.selectFirst("a[href]");
							String redirectpath = java.net.URLDecoder.decode(linkelement.attr("href"));
							redirectpath = "D:\\Downloads\\wikipedia-en-html\\wikipedia-en-html\\en\\articles\\" + redirectpath.substring(21).replace("/", "\\");
							//System.out.println("ID:" + i + ", lang: " + newpath + ", File: " + f.getAbsolutePath() + ", link: " + redirectpath);
							//System.out.println("Searched for: " + redirectpath);
							ScoreDoc[] result = searcherto.search(new TermQuery(new Term("path", redirectpath)), 1).scoreDocs;
							if(result.length == 0) continue;
							int docId = result[0].doc;
							//System.out.println("Found: " + docId + ", Path: " + ensearcher.doc(docId).getField("path"));
							if(clmapping.containsKey(docId)){
								List<Integer> docs = clmapping.get(docId);
								docs.add(i);
								clmapping.put(docId,docs);
							} else {
								List<Integer> docs = new ArrayList<Integer>();
								docs.add(i);
								clmapping.put(docId, docs);
							}
							
						} else {
							//sure it is not redirect
							//todo lookup index id
							//System.out.println("Searched for: " + newpath);
							ScoreDoc[] result = searcherto.search(new TermQuery(new Term("path", newpath)), 1).scoreDocs;
							if(result.length == 0) continue;
							int docId = result[0].doc;
							//int docId = ensearcher.search(new TermQuery(new Term("path", newpath)), 1).scoreDocs[0].doc;
							//System.out.println("Found: " + docId + ", Path: " + ensearcher.doc(docId).getField("path"));
							if(clmapping.containsKey(docId)){
								List<Integer> docs = clmapping.get(docId);
								docs.add(i);
								clmapping.put(docId,docs);
							} else {
								List<Integer> docs = new ArrayList<Integer>();
								docs.add(i);
								clmapping.put(docId, docs);
							}
						}
					}
				}
	    	} catch (Exception e){
	    		try{
	    			e.printStackTrace(pw);
	    		} catch(Exception f){
	    			
	    		}
	    	}
	    }
	    
	    readerfrom.close();
	    readerto.close();
		ObjectMapper mapper = new ObjectMapper();

		//Object to JSON in file
		mapper.writeValue(new File(outputfile), clmapping);
	}

}

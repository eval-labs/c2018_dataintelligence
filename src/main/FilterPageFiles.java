package main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * This class is used to filter out all unnecessary page links and the page map obtained by running PageCount.java.
 * @author Marco Jungwirth
 *
 */
public class FilterPageFiles {

	public static void main(String[] args) throws IOException, ParseException {
		String usage = "Usage:\tFilterPageFile [-input dir] [-output f] [-lang l] [-log f2]\n\n";
	    if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
	      System.out.println(usage);
	      System.exit(0);
	    }
		String input = "D:\\clef\\pagecount.json";
		String logfile = "D:/clef/logs/fpc.log";
		String outputfile = "D:\\clef\\filteredpagecount.json";
		String lang = "eng";
		for(int i = 0;i < args.length;i++) {
	      if ("-input".equals(args[i])) {
	    	  input = args[i+1];
	    	  i++;
	      } else if ("-output".equals(args[i])) {
	    	  outputfile = args[i+1];
	    	  i++;
	      } else if ("-lang".equals(args[i])) {
	    	  logfile = args[i+1];
	    	  i++;
	      } else if ("-log".equals(args[i])) {
	    	  logfile = args[i+1];
	    	  i++;
	      }
		}
		HashMap<String, Integer> pagecount = new HashMap<String, Integer>();
		ObjectMapper mapper = new ObjectMapper();
	    HashMap<String, Integer> pagetable = mapper.readValue(new File(input), HashMap.class);
	    HashMap<String, Integer> result = new HashMap<String, Integer>();
	    for(String key : pagetable.keySet()){
	    	String newKey = key;
	    	Integer i = pagetable.get(key);
	    	if(key.contains(".html#")){
	    		newKey = key.split(".html#")[0] + ".html";
	    	}
	    	
	    	if(i < 5)
	    		continue;
	    	
	    	switch(lang){
	    		case "ger":
	    			if(key.contains("Kategorie~") || 
		               key.contains("Hilfe_Diskussion~") || 
		               key.contains("Hilfe~") || 
		               key.contains("Vorlage~") || 
		               key.contains("Vorlage_Diskussion~") || 
		               key.contains("Wikipedia~") || 
		               key.contains("Wikipedia_Diskussion~") || 
		               key.contains("Benutzer~") || 
		               key.contains("Portal_Diskussion~") || 
		               key.contains("Portal~") || 
		               key.contains("Benutzer_Diskussion~") || 
		               key.contains("Kategorie_Diskussion~") || 
		               key.contains("Bild_Diskussion~") || 
		               key.contains("Bild~") || 
		               key.contains("Diskussion~")){
						continue;
					}
	    			break;
	    		case "fre":
	    			if(key.contains("Catégorie~") || 
		               key.contains("Discussion_Catégorie~") || 
		               key.contains("Utilisateur~") || 
		               key.contains("Discussion_Utilisateur~") || 
		               key.contains("Wikipédia~") || 
		               key.contains("Discussion_Wikipédia~") || 
		               key.contains("Portail~") || 
		               key.contains("Discussion_Portail~") || 
		               key.contains("Projet~") || 
		               key.contains("Discussion_Projet~") || 
		               key.contains("Modèle~") || 
		               key.contains("Discussion_Modèle~") || 
		               key.contains("Référence~") || 
		               key.contains("Discussion_Référence~") || 
		               key.contains("Image~") || 
		               key.contains("Discussion_Image~") || 
		               key.contains("Discuter~")){
						continue;
					}
	    			break;
	    		default:
	    			if(key.contains("Category~") || 
 		               key.contains("Category_talk~") || 
 		               key.contains("Image_talk~") || 
 		               key.contains("Portal~") || 
 		               key.contains("Portal_talk~") || 
 		               key.contains("Help~") || 
 		               key.contains("Help_talk~") || 
 		               key.contains("Talk~") || 
 		               key.contains("Template~") || 
 		               key.contains("Template_talk~") || 
 		               key.contains("User_talk~") || 
 		               key.contains("User~") || 
 		               key.contains("Wikipedia~") || 
 		               key.contains("Image~") || 
 		               key.contains("Wikipedia_talk~")){
 						continue;
 					}
	    	}
	    	if(result.containsKey(newKey))
	    		result.put(newKey, result.get(newKey) + i);
	    	else
	    		result.put(key, i);
	    }
	    
	    mapper.writeValue(new File(outputfile), result);
	}

}

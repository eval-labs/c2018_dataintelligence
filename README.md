## Implementation of Cross-lingual Explicit Semantic Analysis introduced by Sorg and Cimiano in 2008

In this repository you find an implementation of the system described in the [paper](https://pub.uni-bielefeld.de/publication/2497268) by Sorg and Cimiano in 2008.
The implementation assumes, that a static HTML dump of Wikipedia is used as a base for the indexing process.

Moreover I used the following external libraries:

1. lucene-core-7.3.0.jar, lucene-analyzers-common-7.3.0.jar, lucene-queries-7.3.0.jahr (Lucene used for indexing)
2. jsoup-1.11.3.jar (JSoup used to parse HTML)
3. jackson-core-2.9.5.jar, jackson-databind-2.9.5.jar, jackson-annotations-2.9.5.jar (Jackson library used to store and load JAVA objects as JSON files)

---

## How to use the code

The implementation is separated in a couple of main-methods, which fulfil different purposes. However some of the java file depend on each other, hence for each build of a jar file every other class should be included. (For example ClefIndexing depends on SearchFiles for the Similarity function)
After extracting the Wikipedia static HTML dumps on disc, the first step is to run the python-scripts in the folder python article selection.
The scripts are language specific (english, french or german) and they will ignore Wikipedia specific pages, which should be skipped while creating the index.
The next step is to build a pagelink map using the class PageCount.java.

Usage: PageCount [-input dir] [-output f] [-log f2]

input is the directory of the articles of the Wikipedia  
output is a path to the file, where the map is saved in JSON  
log is a path to a file where exceptions will be printed  

Now we need to filter this page link map to pages with at least 5 occurences of the link. This is done by the class FilterPageFiles.java.

Usage: FilterPageFile [-input f] [-output f] [-lang l] [-log f2]

input is the files created by PageCount's output key.  
output is a filtered version of the page link map (at leas 5 occurrences).  
lang can be either ger, eng or fre. Here the language is used to filter out insignificant page links, because in the pages we need to index there will be links to Wikipedia specific pages, which we wanted to ignore in the python script.  
log is again a path to a logfile where exceptions will be printed.  

Now we can create the indices using the ClefIndexing.java class.

Usage: ClefIndexing [-index INDEX_PATH] [-pt PAGETABLE_PATH] [-docs DOCS_PATH] [-stop LANGUAGE] [-log f]

index is a path to a directory where the index will be stored  
pt is the path to the filtered pagetable created in the previous step  
docs is the path to the folder in which the "articles" folder of the Wikipedia, we want to index, is located. We need this because in the page link map are relative paths to this "articles" folder.  
stop is again a language key which can be ger, fre or eng and is used to select the right stop word list and the right snowball stemmer.  
log is a path to the logfile where exceptions will be printed.  

After creating the indices we need to compute the Cross-lingual mappings between the Wikipedias. The class CrossLingualMapping.java is used for this purpose.

Usage: CrossLingualMapping -indexfrom dir -indexto dir -output file -log file

indexfrom is the directory of the index of the source Wikipedia  
indexto is the directory of the index of the destination Wikipedia  
output is the file in which the mapping is saved as JSON file  
log is a path to the file, where exceptions will be printed  

Now we have all the pieces to compute a run. The class ProcessRun.java is used to conduct the experiment.

Usage: ProcessRun -enindex dir -deindex dir -frindex dir -topic file -dataset dir -output file -clmfretoeng file -clmgertoeng file -topick num -reck num -log file

enindex is the directory of the english Wikipedia index  
deindex is the directory of the german Wikipedia index  
frindex is the directory of the french Wikipedia index  
topic is the path to the topic file  
dataset is the path to the directory where the TELONB dataset is located  
output is the path to the file where the relevant documents will be stored in TREC Format  
clmfretoeng is the path to the file where the cross lingual mapping from french to english is stored  
clmgertoeng is the path to the file where the cross lingual mapping from german to english is stored  
topick is the length of the ESA-vector for the topics  
reck is the length of the eSA-vector for the records  
log is the path to the log file, where exceptions will be stored  

The class DatasetStats.java is used to obtain some statistics about the dataset and is not needed for conducting the experiment.  
The class SearchFiles.java was used to test the similarity function which is defined in it.  